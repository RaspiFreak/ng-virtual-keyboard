import {Injectable} from '@angular/core';
import {ReplaySubject} from 'rxjs/internal/ReplaySubject';

@Injectable({
  providedIn: 'root'
})
export class NgVirtualKeyboardService {

  public shift$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  public capsLock$: ReplaySubject<boolean> = new ReplaySubject<boolean>(1);
  public caretPosition$: ReplaySubject<number> = new ReplaySubject<number>(1);

  private capsLock: boolean = false;
  private shift: boolean = false;

  constructor() {
  }

  public setShift(value: boolean) {
    this.shift = value;
    this.shift$.next(this.shift);

    this.setCapsLock(this.shift);
  }

  public setCapsLock(value: boolean) {
    this.capsLock = value;
    this.capsLock$.next(value);
  }

  public toggleShift(): void {
    this.shift = !this.shift;
    this.shift$.next(this.shift);

    this.setCapsLock(this.shift);
  }

  public toggleCapsLock() {
    this.capsLock = !this.capsLock;
    this.capsLock$.next(this.capsLock);
  }

  public setCaretPosition(position: number) {
    this.caretPosition$.next(position);
  }

  public reset() {
    this.setShift(false);
  }
}
