import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgVirtualKeyboardComponent } from './ng-virtual-keyboard.component';

describe('NgVirtualKeyboardComponent', () => {
  let component: NgVirtualKeyboardComponent;
  let fixture: ComponentFixture<NgVirtualKeyboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgVirtualKeyboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgVirtualKeyboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
