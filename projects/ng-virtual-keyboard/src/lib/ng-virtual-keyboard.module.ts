import {NgModule} from '@angular/core';
import {NgVirtualKeyboardComponent} from './ng-virtual-keyboard.component';
import {NgVirtualKeyboardKeyComponent} from './ng-virtual-keyboard-key/ng-virtual-keyboard-key.component';
import {NgVirtualKeyboardDirective} from './ng-virtual-keyboard.directive';
import {MatButtonModule, MatDialogModule, MatIconModule, MatInputModule} from '@angular/material';
import {FlexLayoutModule} from '@angular/flex-layout';
import {NgVirtualKeyboardService} from './ng-virtual-keyboard.service';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatDialogModule,
    MatIconModule,
    MatInputModule
  ],
  declarations: [
    NgVirtualKeyboardDirective,
    NgVirtualKeyboardComponent,
    NgVirtualKeyboardKeyComponent
  ],
  providers: [
    NgVirtualKeyboardService
  ],
  entryComponents: [
    NgVirtualKeyboardComponent
  ],
  exports: [
    NgVirtualKeyboardDirective,
    NgVirtualKeyboardComponent
  ]
})
export class NgVirtualKeyboardModule {
}
