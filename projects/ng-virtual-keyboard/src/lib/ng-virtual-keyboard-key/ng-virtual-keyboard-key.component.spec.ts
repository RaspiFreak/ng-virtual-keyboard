import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgVirtualKeyboardKeyComponent } from './ng-virtual-keyboard-key.component';

describe('NgVirtualKeyboardKeyComponent', () => {
  let component: NgVirtualKeyboardKeyComponent;
  let fixture: ComponentFixture<NgVirtualKeyboardKeyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgVirtualKeyboardKeyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgVirtualKeyboardKeyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
