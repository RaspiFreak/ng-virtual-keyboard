import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  isSpacer,
  isSpecial,
  notDisabledSpecialKeys, specialKeyIcons, specialKeyTexts
} from "../keyboard-layout";
import {KeyPressInterface} from "../key-press.interface";

@Component({
  selector: 'virtual-keyboard-key',
  templateUrl: './ng-virtual-keyboard-key.component.html',
  styleUrls: ['./ng-virtual-keyboard-key.component.css']
})
export class NgVirtualKeyboardKeyComponent implements OnInit {
  @Input() key: string;
  @Input() disabled: boolean;
  @Output() keyPress = new EventEmitter<KeyPressInterface>();

  public special = false;
  public spacer = false;
  public flexValue: string;
  public keyValue: string;
  public icon: string;
  public text: string;


  constructor() {
  }

  ngOnInit() {
    let multiplier = 1;
    let fix = 0;

    if (this.key.length > 1) {
      this.spacer = isSpacer(this.key);
      this.special = isSpecial(this.key);

      const matches = /^(\w+)(:(\d+(\.\d+)?))?$/g.exec(this.key);

      this.keyValue = matches[1];

      if (matches[3]) {
        multiplier = parseFloat(matches[3]);
        fix = (multiplier - 1) * 4;
      }
    } else {
      this.keyValue = this.key;
    }

    if (this.special) {
      if (specialKeyIcons.hasOwnProperty(this.keyValue)) {
        this.icon = specialKeyIcons[this.keyValue];
      } else if (specialKeyTexts.hasOwnProperty(this.keyValue)) {
        this.text = specialKeyTexts[this.keyValue];
      }
    }

    this.flexValue = `${multiplier * 64 + fix}px`;
  }


  public isDisabled(): boolean {
    if (this.spacer) {
      return true;
    } else if (this.disabled && notDisabledSpecialKeys.indexOf(this.keyValue) !== -1) {
      return false;
    } else {
      return this.disabled;
    }
  }

  /**
   * Method to handle actual "key" press from virtual keyboard.
   *  1) Key is "Special", process special key event
   *  2) Key is "Normal", append this key value to input
   */
  public onKeyPress(): void {
    this.keyPress.emit({special: this.special, keyValue: this.keyValue, key: this.key});
  }

}
