import { TestBed, inject } from '@angular/core/testing';

import { NgVirtualKeyboardService } from './ng-virtual-keyboard.service';

describe('NgVirtualKeyboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NgVirtualKeyboardService]
    });
  });

  it('should be created', inject([NgVirtualKeyboardService], (service: NgVirtualKeyboardService) => {
    expect(service).toBeTruthy();
  }));
});
