import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {keyboardCapsLockLayout, KeyboardLayout} from "./keyboard-layout";
import {KeyPressInterface} from "./key-press.interface";
import {MatDialogRef} from "@angular/material";
import {NgVirtualKeyboardService} from "./ng-virtual-keyboard.service";

@Component({
  selector: 'virtual-keyboard',
  templateUrl: './ng-virtual-keyboard.component.html',
  styleUrls: ['./ng-virtual-keyboard.component.css']
})
export class NgVirtualKeyboardComponent implements OnInit, OnDestroy {
  @ViewChild('keyboardInput') keyboardInput: ElementRef;

  public inputElement: ElementRef;
  public layout: KeyboardLayout;
  public placeholder: string;
  public disabled: boolean;
  public maxLength: number | string;

  private caretPosition: number;
  private shift = false;

  private static setSelectionRange(
    input: any,
    start: number,
    end: number
  ): void {
    if (input.setSelectionRange) {
      input.focus();
      input.setSelectionRange(start, end);

    } else if (input.createTextRange) {
      const range = input.createTextRange();

      range.collapse(true);
      range.moveEnd('character', end);
      range.moveStart('character', start);
      range.select();
    }
  }

  constructor(
    public dialogRef: MatDialogRef<NgVirtualKeyboardComponent>,
    public virtualKeyboardService: NgVirtualKeyboardService
  ) {
  }

  ngOnInit() {
    setTimeout(() => {
      this.keyboardInput.nativeElement.focus();
    }, 0);

    this.virtualKeyboardService.shift$.subscribe((shift: boolean) => {
      this.shift = shift;
    });

    this.virtualKeyboardService.capsLock$.subscribe((capsLock: boolean) => {
      this.layout = keyboardCapsLockLayout(this.layout, capsLock);
    });

    this.virtualKeyboardService.caretPosition$.subscribe((caretPosition: number) => {
      this.caretPosition = caretPosition;

      setTimeout(() => {
        NgVirtualKeyboardComponent.setSelectionRange(this.keyboardInput.nativeElement, caretPosition, caretPosition);
      }, 0);
    });

    if (this.inputElement.nativeElement.value.length) {
      this.virtualKeyboardService.setCaretPosition(this.inputElement.nativeElement.value.length);
    }

    this.maxLength = this.inputElement.nativeElement.maxLength > 0 ? this.inputElement.nativeElement.maxLength : '';

    this.checkDisabled();
  }

  ngOnDestroy() {
    this.virtualKeyboardService.reset();
  }

  /**
   * Method to close virtual keyboard dialog
   */
  public close(): void {
    this.dialogRef.close();
  }

  public updateCaretPosition(): void {
    this.virtualKeyboardService.setCaretPosition(this.keyboardInput.nativeElement.selectionStart);
  }

  public keyPress(event: KeyPressInterface): void {
    if (event.special) {
      this.handleSpecialKey(event);
    } else {
      this.handleNormalKey(event.keyValue);

      this.dispatchEvents(event);

      // Toggle shift if it's activated
      if (this.shift) {
        this.virtualKeyboardService.toggleShift();
      }
    }

    this.checkDisabled();
  }

  private checkDisabled(): void {
    const maxLength = this.inputElement.nativeElement.maxLength;
    const valueLength = this.inputElement.nativeElement.value.length;

    this.disabled = maxLength > 0 && valueLength >= maxLength;
  }

  private handleNormalKey(keyValue: string): void {
    let value = '';

    // We have caret position, so attach character to specified position
    if (!isNaN(this.caretPosition)) {
      value = [
        this.inputElement.nativeElement.value.slice(0, this.caretPosition),
        keyValue,
        this.inputElement.nativeElement.value.slice(this.caretPosition)
      ].join('');

      // Update caret position
      this.virtualKeyboardService.setCaretPosition(this.caretPosition + 1);
    } else {
      value = `${this.inputElement.nativeElement.value}${keyValue}`;
    }

    // And finally set new value to input
    this.inputElement.nativeElement.value = value;
  }

  private handleSpecialKey(event: KeyPressInterface): void {
    switch (event.keyValue) {
      case 'Enter':
        this.close();
        break;
      case 'Escape':
        this.close();
        break;
      case 'Backspace':
        const currentValue = this.inputElement.nativeElement.value;

        // We have a caret position, so we need to remove char from that position
        if (!isNaN(this.caretPosition)) {
          // And current position must > 0
          if (this.caretPosition > 0) {
            const start = currentValue.slice(0, this.caretPosition - 1);
            const end = currentValue.slice(this.caretPosition);

            this.inputElement.nativeElement.value = `${start}${end}`;

            // Update caret position
            this.virtualKeyboardService.setCaretPosition(this.caretPosition - 1);
          }
        } else {
          this.inputElement.nativeElement.value = currentValue.substring(0, currentValue.length - 1);
        }

        // Set focus to keyboard input
        this.keyboardInput.nativeElement.focus();
        break;
      case 'CapsLock':
        this.virtualKeyboardService.toggleCapsLock();
        break;
      case 'Shift':
        this.virtualKeyboardService.toggleShift();
        break;
      case 'SpaceBar':
        this.handleNormalKey(' ');
        break;
    }
  }

  private dispatchEvents(event: KeyPressInterface) {
    const eventInit: KeyboardEventInit = {
      bubbles: true,
      cancelable: true,
      shiftKey: this.shift,
      key: event.keyValue,
      code: `Key${event.keyValue.toUpperCase()}}`,
      location: 0
    };

    // Simulate all needed events on base element
    this.inputElement.nativeElement.dispatchEvent(new KeyboardEvent('keydown', eventInit));
    this.inputElement.nativeElement.dispatchEvent(new KeyboardEvent('keypress', eventInit));
    this.inputElement.nativeElement.dispatchEvent(new Event('input', {bubbles: true}));
    this.inputElement.nativeElement.dispatchEvent(new KeyboardEvent('keyup', eventInit));

    // And set focus to input
    this.keyboardInput.nativeElement.focus();
  }

}
