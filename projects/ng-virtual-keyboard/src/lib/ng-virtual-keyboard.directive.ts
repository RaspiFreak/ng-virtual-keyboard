import {Directive, ElementRef, HostListener, Input} from '@angular/core';

import {NgVirtualKeyboardComponent} from "./ng-virtual-keyboard.component";
import {MatDialog, MatDialogRef} from "@angular/material";
import {
  alphanumericKeyboard,
  alphanumericNordicKeyboard,
  extendedKeyboard,
  extendedNordicKeyboard,
  KeyboardLayout,
  numericKeyboard,
  phoneKeyboard
} from "./keyboard-layout";

@Directive({
  selector: '[ng-virtual-keyboard]'
})
export class NgVirtualKeyboardDirective {

  private opened = false;
  private focus = true;

  @Input('ng-virtual-keyboard-layout') layout: KeyboardLayout | string;
  @Input('ng-virtual-keyboard-placeholder') placeholder: string;

  @HostListener('window:blur')
  onWindowBlur() {
    this.focus = false;
  }

  @HostListener('window:focus')
  onWindowFocus() {
    setTimeout(() => {
      this.focus = true;
    }, 0);
  }

  @HostListener('focus')
  onFocus() {
    this.openKeyboard();
  }

  @HostListener('click')
  onClick() {
    this.openKeyboard();
  }

  constructor(
    private element: ElementRef,
    private dialog: MatDialog
  ) {
  }

  private openKeyboard() {
    if (!this.opened && this.focus) {
      this.opened = true;
      let dialogRef: MatDialogRef<NgVirtualKeyboardComponent>;

      dialogRef = this.dialog.open(NgVirtualKeyboardComponent);
      dialogRef.componentInstance.inputElement = this.element;
      dialogRef.componentInstance.layout = this.getLayout();
      dialogRef.componentInstance.placeholder = this.getPlaceHolder();

      dialogRef
        .afterClosed()
        .subscribe(() => {
          setTimeout(() => {
            this.opened = false;
          }, 0);
        });
    }
  }

  private getLayout(): KeyboardLayout {
    let layout;

    switch (this.layout) {
      case 'alphanumeric':
        layout = alphanumericKeyboard;
        break;
      case 'alphanumericNordic':
        layout = alphanumericNordicKeyboard;
        break;
      case 'extended':
        layout = extendedKeyboard;
        break;
      case 'extendedNordic':
        layout = extendedNordicKeyboard;
        break;
      case 'numeric':
        layout = numericKeyboard;
        break;
      case 'phone':
        layout = phoneKeyboard;
        break;
      default:
        layout = this.layout;
        break;
    }
    return layout;
  }

  private getPlaceHolder(): string {
    return this.placeholder ? this.placeholder : this.element.nativeElement.placeholder;
  }
}
